﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]private GameObject[] m_PooledObject;
    [SerializeField] private int m_PoolSize = 10;
    private bool m_switchWeapon;
    private GameObject m_item;

    private List<PoolItem> m_Pool;

    // Use this for initialization
    private void Start()
    {
        m_Pool = new List<PoolItem>();
    }
    void Update()
    {
        if (m_switchWeapon)
        {
            GameObject axe;
            for (int i = 0; i < m_PoolSize; i++)
            {

                axe = Instantiate(m_PooledObject[0]);
                AddItem(axe.GetComponent<PoolItem>());
                m_Pool[i].Pool = this;
                m_switchWeapon = false;
            }
        }
        else if (!m_switchWeapon)
        {
            GameObject knife;
            for (int i = 0; i < m_PoolSize; i++)
            {

                knife = Instantiate(m_PooledObject[1]);
                AddItem(knife.GetComponent<PoolItem>());
                m_Pool[i].Pool = this;
                m_switchWeapon = true;
            }
        }
    }



    public void AddItem(PoolItem item)
    {
        m_Pool.Add(item);
        item.transform.parent = transform;
        item.gameObject.SetActive(false);
    }

    public GameObject InstantiateItem(Vector3 position, Quaternion rotation, Transform parent = null)
    {

        if (m_Pool.Count <= 0)
        {
            Debug.Log("Pool is empty");
            return null;
        }
       
        if(m_switchWeapon == true) {
            m_Pool[0].Init(position, rotation, parent);
            m_item = m_Pool[0].gameObject;
            m_switchWeapon = false;
        }
        else if(m_switchWeapon == false)
        {
            m_Pool[1].Init(position, rotation, parent);
            m_item = m_Pool[1].gameObject;
            m_switchWeapon = true;
        }
        
        m_Pool.RemoveAt(0);
        return m_item;
    }
}
