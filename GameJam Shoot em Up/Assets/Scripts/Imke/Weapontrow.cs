﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapontrow : MonoBehaviour

{

    [SerializeField] private Rigidbody2D m_rigidBody2D;
    [SerializeField] private float m_bulletSpeed;

  
    // Update is called once per frame
    void Update()
    {
        BulletMovement();
        
    }

    private void BulletMovement()
    {
        m_rigidBody2D.velocity = transform.up * m_bulletSpeed;
    }
}
