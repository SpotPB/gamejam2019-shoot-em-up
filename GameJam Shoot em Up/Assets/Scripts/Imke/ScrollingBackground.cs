﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour

{
    private Rigidbody2D m_rigidbody2D;

    public float m_beginScrollSpeed;
    public float m_addSpeed;
    public float m_cooldown;
    public float m_timer;

    void Start()
    {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        CountDown();

        m_rigidbody2D.velocity = new Vector2(0,-m_beginScrollSpeed);

        if (m_timer == 0)
        {
            m_timer = m_cooldown;
            m_rigidbody2D.velocity = new Vector2(0,m_beginScrollSpeed += m_addSpeed);
        }
    }
    public void CountDown()
    {
        if (m_timer > 0f)
        {
            m_timer -= Time.deltaTime;
        }
        if (m_timer < 0f)
        {
            m_timer = 0;
        }

    }
}
