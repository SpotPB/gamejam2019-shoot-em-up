﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour
{
    /*recoursec:
     * Flappy Bird YouTube vid from Unity
     * https://www.youtube.com/watch?v=AyNhGthBFdg
    */

    private BoxCollider2D m_backgroundCollider;
    public float m_groundVerticalLength = 12f;

    void Start()
    {
         m_backgroundCollider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (transform.position.y < -m_groundVerticalLength)
        {
            RepositionBackGround();
        }
    }
    private void RepositionBackGround()
    {
        Vector2 backgroundOffSet = new Vector2(0,m_groundVerticalLength * 2f);
        transform.position = (Vector2)transform.position + backgroundOffSet;
    }
}
