﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePlayerWeapon : MonoBehaviour
{
    [SerializeField] private float m_weaponSpeed =5f;

    
    void Update()
    {
        transform.Translate(Vector2.up * Time.deltaTime * m_weaponSpeed);
    }
}
