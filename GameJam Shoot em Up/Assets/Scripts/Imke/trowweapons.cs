﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trowweapons : MonoBehaviour
{
    [SerializeField] private ObjectPool m_BulletPool;
  
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_BulletPool.InstantiateItem(transform.position, Quaternion.identity);
        }
    }
}
