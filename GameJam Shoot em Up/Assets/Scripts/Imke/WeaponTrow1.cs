﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTrow1 : PoolItem
{
    float m_DeathTimer;
    public float m_BulletSpeed;

    protected override void Reset()
    {
        m_DeathTimer = 0.0f;
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * Time.deltaTime * m_BulletSpeed);
        m_DeathTimer += Time.deltaTime;
        if (m_DeathTimer >= 3.0f)
        {
            ReturnToPool();
        }
    }
}
