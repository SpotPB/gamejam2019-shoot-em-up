﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour


{
    [SerializeField] private GameObject[] m_weaponOption;
    [SerializeField] private float m_weaponSpeed;
    private bool m_switchWeapon;

    private Animator m_animator;
    PlayerState m_playerstate;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_animator.SetInteger("state", (int)m_playerstate);
        m_playerstate = PlayerState.walking;
    }
    void Update()

    {
        //if(Input.GetMouseButton(0))
        //{
        //    switch (m_playerstate)
        //    {
        //        case PlayerState.trow_axe:

        //            TrowAxe();
        //            break;

        //        case PlayerState.trow_knife:

        //            TrowKnife();
        //            break;

        //        default:
        //            print("State not found");
        //            break;


        //    }
        //}




        m_animator.SetInteger("state", (int)m_playerstate);
        m_playerstate = PlayerState.walking;
        if (Input.GetMouseButtonDown(0))
        {
            if (m_switchWeapon)
            {

                //axe

                m_playerstate = PlayerState.trow_axe;
                GameObject clone = m_weaponOption[1];
                Instantiate(clone, transform.position, Quaternion.identity);

                //Destroy(clone, 5);
                m_switchWeapon = false;
            }
            else if (!m_switchWeapon)
            {
                // knife

                m_playerstate = PlayerState.trow_knife;
                GameObject clone = m_weaponOption[0];
                Instantiate(clone, transform.position, Quaternion.identity);

                m_switchWeapon = true;
            }
        }
    }

    private void TrowKnife()
    {
        m_playerstate = PlayerState.trow_axe;
        m_animator.SetInteger("state", (int)m_playerstate);
        GameObject clone = m_weaponOption[1];
        Instantiate(clone, transform.position, Quaternion.identity);
       

    }
    private void TrowAxe()
    {
        m_playerstate = PlayerState.trow_knife;
        m_animator.SetInteger("state", (int)m_playerstate);
        GameObject clone = m_weaponOption[0];
        Instantiate(clone, transform.position, Quaternion.identity);
    }



}

