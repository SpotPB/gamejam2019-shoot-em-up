﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum PlayerState
{
   
    trow_axe = 1,
    trow_knife =0,
    walking = 3

}

public class PlayerController : MonoBehaviour
    
{
    [SerializeField] private RawImage m_life1;
    [SerializeField] private RawImage m_life2;
    [SerializeField] private RawImage m_life3;
    [SerializeField] private Camera m_camera;
    [SerializeField] private Renderer m_playerRenderer;
    

  private int m_currentHealth;
    [SerializeField] private int m_startingHealth = 15;
    private bool m_damaged;
    private bool m_isDeath;
   

    private float m_maxHeight;
    private float m_maxWidth;
    // Start is called before the first frame update
    void Start()
    {
        if (m_camera ==  null)
        {
            m_camera = Camera.main;
        }
        Cursor.visible = false;
        m_currentHealth = m_startingHealth;
    }

    private void FixedUpdate()
    {
        Move();
        
    }

    private void Move()
    {
        float playerHeight = m_playerRenderer.bounds.extents.y;
        float playerWidth =  m_playerRenderer.bounds.extents.x;

        Vector3 rawPos = m_camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 targetPos = new Vector2(rawPos.x, rawPos.y);

        var upperCorner = new Vector2(Screen.width, Screen.height);
        var rawMaxLength = m_camera.ScreenToWorldPoint(upperCorner);
        m_maxHeight = rawMaxLength.y - playerHeight;
        m_maxWidth = rawMaxLength.x - playerWidth;

        float targetHeight = Mathf.Clamp(targetPos.y, -m_maxHeight, m_maxHeight);
        float targetWidth = Mathf.Clamp(targetPos.x, -m_maxWidth, m_maxWidth);
        targetPos = new Vector2(targetWidth, targetHeight);

        GetComponent<Rigidbody2D>().MovePosition(targetPos);

    }

    public void DamageTaken(int amount)
    {
        m_damaged = true;
        m_currentHealth -= amount;
        if(m_currentHealth<= 10)
        {
            m_life3.enabled = false;
        }
        else if(m_currentHealth <=5)
        {
            m_life2.enabled = false;
        }
        else if(m_currentHealth <=0 )
        {
            m_life1.enabled = false;
            Death();
            
        }

        

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if(collision.transform.name.Contains("Bullet"))
        {
            Debug.Log("damage taken");
            DamageTaken(1);
        }
    }
    private void Death()
    {
        
        Debug.Log("Game Over");
    }

   
}
