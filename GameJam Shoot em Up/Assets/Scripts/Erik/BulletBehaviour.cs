﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireTypes
{
    Shotgun,
    MachineGun,
    Pistol
}

public class BulletBehaviour : MonoBehaviour
{
    [SerializeField] FireTypes FireType;
    [SerializeField] int FireSpeed, ProjectileCount;
    [SerializeField] GameObject Projectile;
    [SerializeField] GameObject Player;
    private int PoolSize;
    private float angle;
    private List<GameObject> Bullets = new List<GameObject>();
    private float AmountFired;
    private float Cooldown;
    public float health = 5;
    // Start is called before the first frame update
    void Start()
    {
        PoolSize = (7 * ProjectileCount) - (FireSpeed * ProjectileCount);
        for (int i = 0; i < PoolSize; i++)
        {
            GameObject temp = Instantiate(Projectile);
            temp.transform.parent = transform;
            Bullets.Add(temp);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Player == null)
        {
            Player = GameObject.Find("Player");
        }
        if (health == 0)
        {
            this.gameObject.SetActive(false);
        }
        Cooldown += Time.deltaTime;
        switch (FireType)
        {
            case FireTypes.Shotgun:
                if (Cooldown >= FireSpeed)
                {
                    for (int i = 0; i < Bullets.Count; i++)
                    {
                        if (Bullets[i].activeInHierarchy == false && AmountFired < ProjectileCount)
                        {
                            if (Bullets[i + ProjectileCount - 1] != null && Bullets[i + ProjectileCount - 1].activeInHierarchy == false)
                            {

                                Vector3 heading = transform.forward - transform.position;
                                Vector3 Direction = heading / heading.magnitude;
                                float angle = 2f / ProjectileCount;
                                Debug.Log(angle);
                                Direction.x -= (angle * (ProjectileCount / 2));
                                float delayItem = 0 - (angle * (ProjectileCount / 2));
                                for (int j = i; j < i + (ProjectileCount); j++)
                                {
                                    Bullets[j].SetActive(false);
                                    Bullets[j].GetComponent<BulletData>().LifeTime = 0;
                                    Bullets[j].SetActive(true);
                                    Bullets[j].transform.position = transform.position;
                                    Bullets[j].GetComponent<BulletData>().Direction = Direction;
                                    Bullets[j].GetComponent<BulletData>().Speed = 5;
                                    Direction.x = Direction.x + angle;
                                    Bullets[j].GetComponent<BulletData>().angle = Direction.x;
                                    Bullets[j].GetComponent<BulletData>().Delay = Mathf.Abs(delayItem / 2);
                                    delayItem += angle;
                                    AmountFired += 1;

                                }
                            }
                        }
                    }
                    Cooldown = 0;
                    AmountFired = 0;
                }
                //shotgun stuff
                break;
            case FireTypes.MachineGun:
                if (Cooldown >= FireSpeed)
                {
                    for (int i = 0; i < Bullets.Count; i++)
                    {
                        if (Bullets[i].activeInHierarchy == false && AmountFired < ProjectileCount)
                        {
                            Bullets[i].SetActive(true);
                            Vector3 heading = Player.transform.position - transform.position;
                            Vector3 Direction = heading / heading.magnitude;
                            Bullets[i].transform.position = transform.position;
                            Bullets[i].GetComponent<BulletData>().Direction = Direction;
                            Bullets[i].GetComponent<BulletData>().Speed = 5;
                            if (AmountFired == 0)
                            {
                                Bullets[i].GetComponent<BulletData>().Delay = AmountFired;
                            }
                            else
                            {
                                Bullets[i].GetComponent<BulletData>().Delay = AmountFired / 10;
                            }
                            AmountFired += 1;

                        }
                    }
                    AmountFired = 0;
                    Cooldown = 0;
                }
                //machinegun stuff
                break;
            case FireTypes.Pistol:
                //Pistol Stuff
                break;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.name.Contains("axe"))
        {
            Debug.Log("hit");
            health -= 1;
        }
        if (collision.transform.name.Contains("knife"))
        {
            Debug.Log("hit"); 
            health -= 1;
        }
    }
}
