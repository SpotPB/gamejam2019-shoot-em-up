﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public GameObject[] row1 = new GameObject[5];
    public GameObject[] row2 = new GameObject[5];
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < row1.Length; i++)
        {
            if(row1[i] != null)
            {
                if (row1[i].activeInHierarchy == false)
                {
                    row1[i] = Instantiate(row1[i]);
                }
                row1[i].transform.position = Vector3.Lerp(row1[i].transform.position, new Vector3(i - 2, 1.3f,-2), Time.deltaTime);
            }
        }
    }
}
