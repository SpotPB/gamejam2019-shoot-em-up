﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    public float LifeTime;
    public float Speed;
    public Vector3 Direction;
    public float Delay;
    private float DelayTimer;
    public float angle;

    private void Update()
    {
        DelayTimer += Time.deltaTime;
        if (gameObject.activeSelf && DelayTimer >= Delay)
        {
            LifeTime += Time.deltaTime;
            transform.position += Direction * Speed * Time.deltaTime;
            if (LifeTime >= 3f)
            {
                gameObject.SetActive(false);
                LifeTime = 0;
                DelayTimer = 0;
            }
        }
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.GetComponent<PlayerController>() != null)
        {
            collision.transform.GetComponent<PlayerController>().DamageTaken(1);
        }
    }
}
